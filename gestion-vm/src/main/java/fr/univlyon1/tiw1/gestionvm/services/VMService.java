package fr.univlyon1.tiw1.gestionvm.services;

import fr.univlyon1.tiw1.gestionvm.models.VM;
import fr.univlyon1.tiw1.gestionvm.models.VMDTO;
import fr.univlyon1.tiw1.gestionvm.models.VMRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Class holding VM management code.
 */
@Service
@Slf4j
public class VMService {
    @Autowired
    private VMRepository vmRepository;

    /**
     * Creates a new VM and give it an id.
     *
     * @param vmdto the VM's information
     * @return vmdto updated with the VM's id.
     */
    @Transactional
    public VMDTO createVM(VMDTO vmdto) {
        VM vm = vmdto.asVM();
        vm.setId(null);
        vm = vmRepository.save(vm);
        return vm.asDTO();
    }

    /**
     * Retreive all the VMs
     */
    public Collection<VMDTO> getAllVMs() {
        return vmRepository
                .findAll()
                .stream()
                .map(VM::asDTO)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public VMDTO getVM(long id) throws NoSuchVMException {
        try {
            VM vm = vmRepository.getOne(id);
            if (vm == null) {
                throw new NoSuchVMException("VM " + id + " not found");
            }
            return vm.asDTO();
        } catch (EntityNotFoundException e) {
            throw new NoSuchVMException("VM " + id + " not found", e);
        }
    }
}
